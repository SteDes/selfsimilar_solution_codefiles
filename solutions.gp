##########################################################################################
# This program is free software: you can redistribute it and/or modify                   #
# it under the terms of the GNU General Public License as published by                   #
# the Free Software Foundation, either version 3 of the License, or                      #
# (at your option) any later version.                                                    #
#                                                                                        #
# This program is distributed in the hope that it will be useful,                        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                          #
# GNU General Public License for more details.                                           #
#                                                                                        #
# You should have received a copy of the GNU General Public License                      #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.                  #
# 											 #
#	 © Stephan Deschner; sdeschner@astrophysik.uni-kiel.de				 #
##########################################################################################

#!/usr/bin/gnuplot -persist


reset
set macros

set style line 1 linecolor rgbcolor "#004f7c" lw 3
set style line 2 linecolor rgbcolor "#8c969d" lw 3
set style line 3 linecolor rgbcolor "#57A1AD" lw 3


### initial parameters

alpha = 1.0
kappa = 0.5

### copy the RHC here
x0 = 1.53131e+00
uu = 4.54340e-01
ud = 6.02732e-01
Su = 1.15499e+00
Sd = 1.33951e+00

### define the times for output (time is scale parameter, similarity variable)
tau1 = 1
tau2 = 3
tau3 = 6


### define the ode system

dv(t,v) = (v*alpha-kappa*t)/(t*((t-v)^2.-1.));
ds(t,s,v) = (s*v*(alpha-kappa)*(t-v)-s*kappa)/(t*((t-v)^2.-1.));

rhs(func_str)=sprintf("%s",func_str[strstrt(func_str,"=")+1:])

odesys=sprintf("v' = %s; s' = %s",rhs(GPFUN_dv),rhs(GPFUN_ds))

solvemin(vd0,sd0,t0,tmin)=sprintf("v=%e; s=%e; print t,v,s; step %e,%e",vd0,sd0,t0,tmin);
solvemax(vu0,su0,t0,tmax)=sprintf("v=%e; s=%e; print t,v,s; step %e,%e",vu0,su0,t0,tmax);

### removes blank line and reverse the entries from "jump to zero" to "zero to jump" for printing the discontinuity

awk="| awk '{if ($0 != \"\") print $0;}'"
rev = " | awk '{a[i++]=$0} !NF {print NR;b[k++]=NR;} END {for(j=b[0]-2;j>=0;) print a[j--]; for(l=b[0];l<=b[1];)print a[l++]}'"

### solve the ode system

solveode (alpha,kappa,vu0,su0,vd0,sd0,tmin,t0,tmax) = \
          sprintf ('<echo "alpha=%f;kappa=%f;%s;%s;%s;"\
          | ode -A -r 1e-10 -p 16 %s',\
          alpha,kappa,odesys,solvemin(vd0,sd0,t0,tmin),solvemax(vu0,su0,t0,tmax),rev)


### define Macros

TMARGIN = "set tmargin at screen 0.95; set bmargin at screen 0.55"
BMARGIN = "set tmargin at screen 0.55; set bmargin at screen .15"
LRMARGIN = "set lmargin at screen 0.1; set rmargin at screen 0.9"

set key top right samplen 1
set xrange [0.0:15]
graph(n) = sprintf('$\tau=%1.1f$',n)

### plotting the odesys

xmin = 0.001
xmax = 20

set multiplot layout 1,2 upwards
@TMARGIN;@LRMARGIN;

set yrange [0:.8];
set ylabel '$u\left(r, \tau\right)$';
set format x '';

plot solveode(alpha,kappa,uu,Su,ud,Sd,xmin,x0,xmax) using ($1*tau1):2 ls 2 t '' w l,\
     solveode(alpha,kappa,uu,Su,ud,Sd,xmin,x0,xmax) using ($1*tau2):2 ls 3 t '' w l,\
     solveode(alpha,kappa,uu,Su,ud,Sd,xmin,x0,xmax) using ($1*tau3):2 ls 1 t '' w l

@BMARGIN;
set format x;
set yrange [0:1.5];
set ylabel '$\rho\left(r, \tau\right)$';
set xlabel '$r$';

plot solveode(alpha,kappa,uu,Su,ud,Sd,xmin,x0,xmax) using ($1*tau1):($3*$1**(-kappa)) ls 2 t graph(tau1) w l,\
     solveode(alpha,kappa,uu,Su,ud,Sd,xmin,x0,xmax) using ($1*tau2):($3*$1**(-kappa)) ls 3 t graph(tau2) w l,\
     solveode(alpha,kappa,uu,Su,ud,Sd,xmin,x0,xmax) using ($1*tau3):($3*$1**(-kappa)) ls 1 t graph(tau3) w l

unset multiplot

### EOF
