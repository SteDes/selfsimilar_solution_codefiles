#! /usr/bin/python
# -*- coding: utf-8 -*-

##########################################################################################
# This program is free software: you can redistribute it and/or modify			 #
# it under the terms of the GNU General Public License as published by			 #
# the Free Software Foundation, either version 3 of the License, or			 #
# (at your option) any later version.							 #
# 											 #
# This program is distributed in the hope that it will be useful,			 #
# but WITHOUT ANY WARRANTY; without even the implied warranty of			 #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the				 #
# GNU General Public License for more details.						 #
# 											 #
# You should have received a copy of the GNU General Public License			 #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.			 #
#                                                                                        #
#        © Stephan Deschner; sdeschner@astrophysik.uni-kiel.de                           #
##########################################################################################

import numpy as np
import scipy as sp
from scipy.integrate import odeint


print("##################################################################\n")
print("# computation of the isothermal Rankine-Hugoniot jump-conditions #\n")
print("# © Stephan Deschner sdeschner@astrophysik.uni-kiel.de 		 #\n")
print("## python3 alternative ###########################################\n\n")

rtol = 1E-6
atol = 1E-6

print ("specify variables\n ");
system = int (input ("1 for implosion, 2 for explosion \n "));
alpha = int (input ("coordinate alpha in 0, 1, 2: \n "));
kappa = float (input ("IC density exponent kappa >= 0: \n"));

S00 = 1.;
tmin = 1.E-3;

accuracy = 1.E+5;
epsilon = 1.E-4;

def p(a, k):
    if (a == 0 or a == k):
        p0 = 2.; 
    else:
        p0 = a / (a - k);

    if (k >= 2.):
        p0 = 3.;
    
    return p0

t_start = p (alpha, kappa);

# boundary conditions u0
def g(a, k, x):
    u_0 = k * x / (a + 1.);
    return u_0

u0 = [g(alpha,kappa,tmin), S00];

if (system == 1):
    u00 = [-1.0, S00];
else:
    u00 = [0.0, S00];

tend = p(alpha, kappa);
ttemp = tend;

print ("\n....... running exact RHC ......\n")

def f (y,t,a,k):
        u,S = y
        dydt = [(a*u-k*t)/(t*((t-u)**2.-1.)),(u*S*(a-k)*(t-u)-S*k)/(t*((t-u)**2.-1.))]
        return dydt

def s (y,t,a,k):
        u,S = y
        dydt = [(k - u * a * t) / ((1. - t * u)**2. - t**2), (S*k*t - S*u*(a-k) * (1. - u*t)) / ((1. - t*u)**2. - t**2)]
        return dydt

eta1 = np.linspace (0.0, 1./tend, accuracy);
u_eta = odeint (s, u00, eta1, (alpha, kappa,), rtol=rtol, atol=atol)
utemp = u_eta[-1,0]
eps = 1.;

while (eps > epsilon):
    td = np.linspace (tmin, tend,accuracy);
    
    yd = odeint (f, u0, td, (alpha, kappa,), rtol=rtol, atol=atol)
    u_u = (1. + yd[-1,0] * td[-1] - td[-1]**2.) / (yd[-1,0] - td[-1]);

    tu = np.linspace (0, 1 / td[-1], accuracy);

    yu = odeint (s, u00, tu, (alpha, kappa,), rtol=rtol, atol=atol)
    Su = yu[-1,1]

    Sd = Su / (yd[-1,0] - td[-1])**2.;

    eps = abs ((yu[-1,0] - (1. + yd[-1,0] * td[-1] - td[-1]**2.) / (yd[-1,0] - td[-1])) / (yu[-1,0]));

    if (eps > epsilon):
        if (u_u > utemp):
            tend = td[-1] * (1. - 5. * epsilon);
        else:
            tend = (1. + 5. * epsilon) * td[-1];

        utemp = yu[-1,0];



# output

print(" RB: v_0=%f und S_0=%f\n RB: v_00=%f und S_00=%f\n epsilon=%f\n start jumppos=%f\n system=%f\n alpha=%f\n kappa=%f\n"
      % (u0[0], u0[1],u00[0],u00[1],epsilon,ttemp,system,alpha,kappa))

print("\n_____|solutions|_________________________________\n")
print ("x0 = %1.5e\n" % tend)
print ("uu = %1.5e\n" % yu[-1,0])
print ("ud = %1.5e\n" % yd[-1,0])
print ("Su = %1.5e\n" % Su)
print ("Sd = %1.5e\n" % Sd)
print("_________________________________________________\n") 


# EOF


