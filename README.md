# Useful files for Self-similar solutions to isothermal shockkproblems

All scriptfiles are published under gnu GPL v3. See the headers of the files.

This repository involves some useful scriptfiles to produce and reproduce the solutions investigated
in the paper [Self-similar solutions to isothermal shock problems](http://adsabs.harvard.edu/abs/2018SJAM...78...80D) (published in 
SIAM Journal of Applied Mathematics, Vol. 78, No. 1, pp.80-103 (2018)).

The octave scriptfile **computeRHC** computes the isothermal Rankine-Hugoniot jumpconditions (RHC) 
to close the solutionbranches to isothermal shockproblems, meaning isothermal implosions
and explosions. There are two gnuplot script files available to plot the **phaseplane** 
or the complete **solutions** to the shockproblems.

---
# NEW

New Python (3.7) and jupyter notebook tranlations with singular point and eigenvalue problem analysis. 

---
# Necessities

The script files solve the RHC, and the solutions to isothermal shock problems. The solutions may then be plottet in a phaseplane to investigate their behavior, or they may be plottet in one graph.
On the upper half the velocity and on the lower the density in dependence of the radial coordinate and time. Please have a look at the above mentioned paper for details, e.g. for valid values to the problems.

The preconditions for the usage of the scriptfiles are:

- a version of [gnu Octave](https://www.gnu.org/software/octave/)
- a version of [gnuplot](http://www.gnuplot.info)
- a version of gnu's [plotutils](https://www.gnu.org/software/plotutils/) for the usage of ode
        
Above mentioned programmes are available for presumeably every linux machine but also for macOS via macports or homebrew. Maybe you have to install XCode on macOs for compiling gnu octave and the other programs.

Otherwise the code should easily get translated to python syntax. Please use the description in the paper for
details of what the code is doing.

---

# Manual

Clone this repository and navigate to the files in your console.

## Compute the RHC
 
- type **octave computeRHC**
- read the programs output and enter whether

        - RHC for implosion (1)
        - RHC for explosion (2)
        - the coordinate (cartesian 0, polar 1, spherical 2)
        - the value for the exponent kappa (see the paper for valid values)
        
- wait until the solutions have converged

The code may need some minutes. Please feel free to control the computation by using `htop` or your `systemcontrol` by checking the CPU usage. For aborting the compution press `ctrl+c` as usual

---

## Plot the phaseplane
... of the solution for the velocity equation using the gnuplot scriptfile `phaseplane.gp`

- open gnuplot and type **load 'phaseplane.gp'**
- edit the file to change the parameters at line 87
        
        - alpha
        - kappa
        
- the function solveode(kappa, alpha, xi_0, u_0, tmin, t0, tmax) in the plot command (line 111) handles the initial conditions 
        
        - xi_0: initial condition for spatial value xi
        - u_0 : initial condition for velocity solution u
        - tmin: integration from tmin until t0
        - t0  : when a singular line is reached (function loses its uniqueness, disjunct regions in phaseplane)
        - tmax: integration from t0 until tmax

You may use different values `xi_0` etc. for different values of alpha and kappa for viewing possible solutions.

---

## Plot all solutions
... for both, the density and the velocity using the gnuplot scriptfile `solutions.gp`
        
- run **octave computeRHC** for your parameters
- open gnuplot and type **load 'solutions.gp'**
- edit and replace the predefined values from line 35 to 39 with your computed ones
- you may adjust the x/y-ranges as well as the times 
- adjust the xmin/xmax values

---        

Please feel free to customize the scriptfiles as needed and to comment and give advice to 
[sdeschner@astrophysik.uni-kiel.de](http://www.astrophysik.uni-kiel.de/de/mitarbeiter/dipl.-phys.-stephan-deschner)
