##########################################################################################
# This program is free software: you can redistribute it and/or modify                   #
# it under the terms of the GNU General Public License as published by                   #
# the Free Software Foundation, either version 3 of the License, or                      #
# (at your option) any later version.                                                    #
#                                                                                        #
# This program is distributed in the hope that it will be useful,                        #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                         #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                          #
# GNU General Public License for more details.                                           #
#                                                                                        #
# You should have received a copy of the GNU General Public License                      #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.                  #
#----------------------------------------------------------------------------------------#
# This script was originally made by 							 #
#    Tobias Illenseer (tillense@astrophysik.uni-kiel.de)				 #
# and adapted to isothermal implosions and explosions by 				 #
#    Stephan Deschner (sdeschner@astrophysik.uni-kiel.de)				 #
# 											 #
#	 © Stephan Deschner; sdeschner@astrophysik.uni-kiel.de				 #
##########################################################################################

#!/usr/bin/gnuplot -persist

reset

set macros

aspect=0.75

red=sprintf("#8A060C")
blue=sprintf("#004f7c")

set style arrow 1 nohead lt 1 lc rgb 'black'
set style arrow 2 head lt 1 lc rgb 'black'

set xlabel '$\xi$'
set ylabel '$u\left(\xi\right)$'

### xranges and yranges
xmin=-1.0
xmax=6.0
ymin=-1.
ymax=1.5

### number of sample points for arrows
xsamples=33
ysamples=30
vscale=0.18


### define the ode
dx(x,y) = x*((x-y)*(x-y)-1.0)
dy(x,y) = g*y-a*x

### general functions
rhs(func_str)=sprintf("%s",func_str[strstrt(func_str,"=")+1:])

### define autonomous system of 2 ODEs
odesys=sprintf("x' = %s; y' = %s",rhs(GPFUN_dx),rhs(GPFUN_dy))

### commands for numerical solution (uses external program 'ode' from GNU plotutils)
solveneg(x0,y0,t0,tmin)=sprintf("x=%e; y=%e; print t,x,y; step %e,%e",x0,y0,t0,tmin)
solvepos(x0,y0,t0,tmax)=sprintf("x=%e; y=%e; print t,x,y; step %e,%e",x0,y0,t0,tmax)
solveode(a,g,x0,y0,t0,tmin,tmax)=sprintf('<echo "a=%e;g=%e;%s;%s;%s" | ode -A -r 1e-10 -p 16',a,g,odesys,solveneg(x0,y0,t0,tmin),solvepos(x0,y0,t0,tmax))

### commands for drawing the directional field of the ODE
directional_field(dx,dy,a,g,xpts_num,ypts_num,xmin,xmax,ymin,ymax) = \
  sprintf('<echo "set samples %d,%d; set isosamples %d,%d; set table; unset title; dx(x,y)=%s; dy(x,y)=%s; a=%f; g=%f; splot [%f:%f][%f:%f] atan2(dy(x,y),dx(x,y))" | gnuplot',xpts_num,ypts_num,xpts_num,ypts_num,dx,dy,a,g,xmin,xmax,ymin,ymax)#"

df_xyscale(xmin,xmax,ymin,ymax)=abs((ymax-ymin)/(xmax-xmin))
df_vector_length(phi,a,b)=a*b/sqrt((a*sin(phi))**2+(b*cos(phi))**2)
df_vector_specs(aspect,vscale,xmin,xmax,ymin,ymax) = \
  sprintf('($1-0.5*%f*df_vector_length($3,%f,%f)*cos($3)):($2-0.5*%f*df_vector_length($3,%f,%f)*sin($3)):(%f*df_vector_length($3,%f,%f)*cos($3)):(%f*df_vector_length($3,%f,%f)*sin($3)) w vectors', \
  vscale,aspect,df_xyscale(xmin,xmax,ymin,ymax),vscale,aspect,df_xyscale(xmin,xmax,ymin,ymax),vscale,aspect,df_xyscale(xmin,xmax,ymin,ymax),vscale,aspect,df_xyscale(xmin,xmax,ymin,ymax))


### generate specs for arrows
specs=df_vector_specs(aspect,vscale,xmin,xmax,ymin,ymax)

set key box opaque

### plot vertical singular solution
set arrow 3 from 0,-1 to 0,0.5 nohead lc rgb 'black' lw 2 dt 4


### set parameters
kappa=0.5
alpha=1.0

### singular lines

u1(alpha,kappa,x)=kappa*x/alpha
u2(vz,x)=x+vz*1

### singular points

D1x(alpha,kappa)=0.0
D1y(alpha,kappa)=D1x(alpha,kappa)
D2x(alpha,kappa,vz)=vz*alpha/(alpha-kappa)
D2y(alpha,kappa,vz)=vz*kappa/(alpha-kappa)

### plot directional field, nodes and solutions

plot [xmin:xmax][ymin:ymax] \
     directional_field(rhs(GPFUN_dx),rhs(GPFUN_dy),kappa,alpha,xsamples,ysamples,xmin,xmax,ymin,ymax) u @specs as 2 not, \
     u1(alpha,kappa,x) 	w l dt 4 lc rgb 'black' lw 2 not,\
     u2(+1,x) 		w l dt 4 lc rgb 'black' lw 2 not,\
     u2(-1,x) 		w l dt 4 lc rgb 'black' lw 2 not,\
     u2(0,0)		w l dt 4 lc rgb 'black' lw 2 t 'sing. lines',\
     solveode(kappa,alpha,50.0,-1.0,0,-100.0,0.0) 	u 2:3 w l lc rgb blue lw 2 not,\
     solveode(kappa,alpha,10.0,.25,10,0.0,.0) 		u 2:3 w l lc rgb blue lw 2 not,\
     solveode(kappa,alpha,.001,.001,10,0.0,1.0) 	u 2:3 w l lc rgb blue lw 2 not,\
     solveode(kappa,alpha,50.0,-.5,0,-10.0,0.0) 	u 2:3 w l lc rgb blue lw 2 not,\
     solveode(kappa,alpha,50.0,-2.5,10,0.0,0.0) 	u 2:3 w l lc rgb blue lw 2 not,\
     solveode(kappa,alpha,.5,.5,0,-100.0,2.0) 		u 2:3 w l lc rgb blue lw 3 t 'possible solutions',\
     '+' u (D1x(alpha,kappa)):(D1y(alpha,kappa)) 	w p pt 9 ps 1.5 lc rgb red t 'D_1',\
     '+' u (D2x(alpha,kappa,+1)):(D2y(alpha,kappa,+1)) 	w p pt 7 ps 1.5 lc rgb red not,\
     '+' u (D2x(alpha,kappa,-1)):(D2y(alpha,kappa,-1)) 	w p pt 7 ps 1.5 lc rgb red t 'D_2'

### EOF
